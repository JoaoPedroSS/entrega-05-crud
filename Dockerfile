FROM node:17

WORKDIR /q4

COPY . /q4/

RUN npm install

CMD "npm run dev"