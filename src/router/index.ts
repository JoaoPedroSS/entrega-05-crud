import { Express } from "express";
import { useRouter } from "./user.router";


export const initializerRouter = (app: Express) => {
  app.use("/api", useRouter());
};
