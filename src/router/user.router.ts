import { Router } from "express";

import { create } from "../controllers/createUser.controller";
import { list } from "../controllers/listUsers.controllers";
import { DeleteUserController } from "../controllers/deletedUser.controllers";
import { current } from "../controllers/currentUser.controller";
import { userUpdate } from "../controllers/updateUser.controller";
import { isAuthenticated } from "../middlewares/authenticate.middleware";
import { login } from "../controllers/login.controlller";
import { isAdm } from "../middlewares/userIsAdm";

const router = Router();

export const useRouter = () => {
  router.post("/users", create);
  router.get("/users", isAuthenticated, isAdm, list);
  router.get("/users/profile", isAuthenticated, current);
  router.delete("/users/:uuid", isAuthenticated, new DeleteUserController().handle);
  router.patch("/users/:uuid", isAuthenticated, userUpdate);


  router.post("/login", login);

  return router;
};
