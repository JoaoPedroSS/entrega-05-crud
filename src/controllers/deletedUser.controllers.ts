import { Response, Request, NextFunction } from "express";
import { DeleteUserService } from "../services/user.services";


export class DeleteUserController {
  async handle(request: Request, response: Response) {
      const { uuid } = request.params;

      const service = new DeleteUserService();

      const result = await service.execute(uuid);

      if(result instanceof Error) {
          return response.status(400).json({message: result.message});
      }

      return response.status(204).json({message: "User Deleted!"});

  }
}
