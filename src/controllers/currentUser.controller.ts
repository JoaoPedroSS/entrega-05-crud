import { Response, Request, NextFunction } from "express";
import jwt from "jsonwebtoken";
import { getRepository } from "typeorm";
import { User } from "../entities";



export const current = async (req: Request, res: Response, next: NextFunction) => {

    const userRepository = getRepository(User);

    const token = req.headers.authorization?.split(' ')[1];

    let user:any;

    jwt.verify(token as string, process.env.SECRET as string, async (err: any, decoded: any) => {

        const userUuid = decoded.uuid;

        user = userUuid

        req.user = { uuid: userUuid};
        const userFound = userRepository.findOne(req.user);

        user = await userFound  

        res.send(user);
        next();
    })

    
};
