import { Response, Request } from "express";
import { updateUser } from "../services/user.services";

export const userUpdate = async (req: Request, res: Response) => {
    const { uuid } = req.params;

    const update = await updateUser(uuid, req.body);

    res.send(update);
}