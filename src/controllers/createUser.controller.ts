import { Response, Request, NextFunction } from "express";
import { createUser } from "../services/user.services";

export const create = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const user = await createUser(req.body);
    return res.send(user);
  } catch (err) {
    next(err);
  }
};
