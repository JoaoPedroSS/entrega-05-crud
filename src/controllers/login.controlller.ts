import { Request, Response, NextFunction } from "express";
import { authenticateUser } from "../services/login.services";

export const login = async (req: Request, res: Response, next: NextFunction) => {

  try {
    const { email, password } = req.body;
    const token = await authenticateUser(email, password);
  
  
    res.json({token});
    
  } catch (error) {
    next(error)
  }

};
