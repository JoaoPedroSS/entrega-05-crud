import { Response, Request } from "express";
import { listUser } from "../services/user.services";

export const list = async (req: Request, res: Response) => {

    const users = await listUser();

    res.send(users)

}