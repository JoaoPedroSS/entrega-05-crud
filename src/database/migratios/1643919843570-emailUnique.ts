import {MigrationInterface, QueryRunner} from "typeorm";

export class emailUnique1643919843570 implements MigrationInterface {
    name = 'emailUnique1643919843570'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "users" ADD CONSTRAINT "UQ_97672ac88f789774dd47f7c8be3" UNIQUE ("email")`);
        await queryRunner.query(`ALTER TABLE "users" DROP COLUMN "createdOn"`);
        await queryRunner.query(`ALTER TABLE "users" ADD "createdOn" TIMESTAMP NOT NULL`);
        await queryRunner.query(`ALTER TABLE "users" DROP COLUMN "updatedOn"`);
        await queryRunner.query(`ALTER TABLE "users" ADD "updatedOn" TIMESTAMP NOT NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "users" DROP COLUMN "updatedOn"`);
        await queryRunner.query(`ALTER TABLE "users" ADD "updatedOn" character varying NOT NULL`);
        await queryRunner.query(`ALTER TABLE "users" DROP COLUMN "createdOn"`);
        await queryRunner.query(`ALTER TABLE "users" ADD "createdOn" character varying NOT NULL`);
        await queryRunner.query(`ALTER TABLE "users" DROP CONSTRAINT "UQ_97672ac88f789774dd47f7c8be3"`);
    }

}
