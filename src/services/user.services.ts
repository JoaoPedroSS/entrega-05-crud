import { getRepository, getCustomRepository } from "typeorm";
import UsersRepository from "../repositories/userRepositories";
import { User } from "../entities";
import { ErrorHandler } from "../middlewares/error.middlewares";
interface UserBody {
  email: string;
  password: string;
  name: string;
  isAdm: boolean;
  createdOn: Date;
  updatedOn: Date;
}

export const createUser = async (body: UserBody) => {
  const { email, password, name, isAdm } = body;
  try {
    let date = new Date();
    const userRepository = getRepository(User);

    const user = userRepository.create({
      email,
      password,
      name,
      isAdm,
      createdOn: date,
      updatedOn: date,
    });

    await userRepository.save(user);

    return user;
  } catch (error) {
    throw new ErrorHandler(400, "E-mail already registered");
  }
};

export const listUser = async () => {
  const userRepository = getRepository(User);

  const users = await userRepository.find();

  return users;
};


export const listUserProfile = async (uuid: string) => {
  const userRepository = getRepository(User);

  const users = await userRepository.findOne(uuid);

  return users;
};


export class DeleteUserService {
  async execute (id: string) {
      const repo = getRepository(User);

      if(!(await repo.findOne(id))) {
          return new ErrorHandler(400,"User does not exists!");
      }

      await repo.delete(id);
      
  }
}


export const updateUser = async (idUser: string, data: any) => {
  const userRepository = getCustomRepository(UsersRepository);

  const user = await userRepository.findOne(idUser);

  console.log(user === undefined)

  if(!user) {
    return new ErrorHandler(400, "User not Found!")
  }
  let date = new Date();

  user.updatedOn = date

  return await userRepository.save({
    ...user,
    ...data,
  });
};
