import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
import { getCustomRepository } from "typeorm";
import { ErrorHandler } from "../middlewares/error.middlewares";
import UsersRepository from "../repositories/userRepositories";

export const authenticateUser = async (email: string, password: string) => {
  const userRepository = getCustomRepository(UsersRepository);

  const user = await userRepository.findByEmail(email);

  if (user === undefined || !bcrypt.compareSync(password, user.password)) {
    return new ErrorHandler(401, "Wrong email/password");
  }

  const token = jwt.sign({ uuid: user.uuid }, process.env.SECRET as string, {
    expiresIn: "1d",
  });

  return token;
};
