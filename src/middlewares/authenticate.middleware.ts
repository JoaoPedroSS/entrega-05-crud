import { Request, Response, NextFunction } from "express"
import jwt from "jsonwebtoken";
import { ErrorHandler } from "./error.middlewares";

export const isAuthenticated = (req: Request, res: Response, next: NextFunction) => {

    const token = req.headers.authorization?.split(' ')[1];

    jwt.verify(token as string, process.env.SECRET as string, (err: any, decoded: any) => {
        if(err) {
            return next(new ErrorHandler(400,'Invalid or expired token'));
        }

        const userUuid = decoded.uuid;

        req.user = { uuid: userUuid};

        next();
    })

}