

export class ErrorHandler extends Error {
  statusCode: any;
  constructor(statusCode:any, message:any) {
    super();
    this.message = message;
    this.statusCode = statusCode;
  }
} 


export const handleError = (err:any, res:any) => {
    const { statusCode, message } = err;
    res.status(statusCode).json({
      status: "error",
      statusCode,
      message
    });
  };