import { Request, Response, NextFunction } from "express"
import jwt from "jsonwebtoken";
import { getRepository } from "typeorm";
import { User } from "../entities";
import { ErrorHandler } from "./error.middlewares";

export const isAdm = (req: Request, res: Response, next: NextFunction) => {

    const token = req.headers.authorization?.split(' ')[1];

    jwt.verify(token as string, process.env.SECRET as string, async (err: any, decoded: any) => {
        
        const user  = getRepository(User);

        const userUuid = decoded.uuid;
        
        req.user = { uuid: userUuid};
        
        console.log(userUuid)

        const userFound = await user.findOne(userUuid)
        console.log(userFound)
    
        if(userFound?.isAdm === false) {
            return next(new ErrorHandler(400,'User is not Admin!'));
        }
        next();
    })

}