import "reflect-metadata";
import express from "express";
import { connectDatabase } from "./database";
import { initializerRouter } from "./router/index";
import { handleError } from "./middlewares/error.middlewares";


connectDatabase();

const app = express();

app.use(express.json());

initializerRouter(app);

app.use((err:any, req:any, res:any, next:any) => {
    handleError(err, res)
});
export default app;
